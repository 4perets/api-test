package Project01;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.filter.log.LogDetail;
import com.jayway.restassured.filter.log.RequestLoggingFilter;
import com.jayway.restassured.filter.log.ResponseLoggingFilter;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import com.jayway.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.jayway.restassured.RestAssured.given;
import static junit.framework.Assert.*;


public class FirstProject {

    private RequestSpecification reqSpec =  new RequestSpecBuilder()
            .setContentType(ContentType.JSON)
            .setBaseUri("https://reqres.in/")
            .addFilter(new ResponseLoggingFilter(LogDetail.BODY))
            .addFilter(new ResponseLoggingFilter(LogDetail.STATUS))
            .addFilter(new RequestLoggingFilter(LogDetail.BODY))
            .build();;
    private JSONObject requestParams = new JSONObject();



    @BeforeTest
    public void precondition() {

    }

    @Test
    public void ListUsers() {
        String jsonString = "{\"page\":2,\"per_page\":6,\"total\":12,\"total_pages\":2,\"data\":[{\"id\":7,\"email\":\"michael.lawson@reqres.in\",\"first_name\":\"Michael\",\"last_name\":\"Lawson\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg\"},{\"id\":8,\"email\":\"lindsay.ferguson@reqres.in\",\"first_name\":\"Lindsay\",\"last_name\":\"Ferguson\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg\"},{\"id\":9,\"email\":\"tobias.funke@reqres.in\",\"first_name\":\"Tobias\",\"last_name\":\"Funke\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg\"},{\"id\":10,\"email\":\"byron.fields@reqres.in\",\"first_name\":\"Byron\",\"last_name\":\"Fields\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg\"},{\"id\":11,\"email\":\"george.edwards@reqres.in\",\"first_name\":\"George\",\"last_name\":\"Edwards\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/mrmoiree/128.jpg\"},{\"id\":12,\"email\":\"rachel.howell@reqres.in\",\"first_name\":\"Rachel\",\"last_name\":\"Howell\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/hebertialmeida/128.jpg\"}]}";

               Response res =  given().
                        spec(reqSpec).
               when().
                    get("https://reqres.in/api/users?page=2");

                ResponseBody body = res.body();
                assertEquals(res.statusCode(), 200);
               assertEquals(body.asString(), jsonString);
    }
    @Test
    public void SingleUser(){
        String jsonString = "{\"data\":{\"id\":2,\"email\":\"janet.weaver@reqres.in\",\"first_name\":\"Janet\",\"last_name\":\"Weaver\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg\"}}";
        Response res =  given().
                spec(reqSpec).
                when().
                    get("https://reqres.in/api/users/2");
        ResponseBody body = res.body();
        assertEquals(res.statusCode(), 200) ;
        assertEquals(body.asString(), jsonString);

    }
    @Test
    public void SingleUserNotFound(){
        String jsonString = "{}";
        Response res =  given().
                spec(reqSpec).
                when().
                get("https://reqres.in/api/users/23");
        ResponseBody body = res.body();
        assertEquals(res.statusCode(), 404) ;
        assertEquals(body.asString(), jsonString);

    }

    @Test
    public void ListResource(){
        String jsonString = "{\"page\":1,\"per_page\":6,\"total\":12,\"total_pages\":2,\"data\":[{\"id\":1,\"name\":\"cerulean\",\"year\":2000,\"color\":\"#98B2D1\",\"pantone_value\":\"15-4020\"},{\"id\":2,\"name\":\"fuchsia rose\",\"year\":2001,\"color\":\"#C74375\",\"pantone_value\":\"17-2031\"},{\"id\":3,\"name\":\"true red\",\"year\":2002,\"color\":\"#BF1932\",\"pantone_value\":\"19-1664\"},{\"id\":4,\"name\":\"aqua sky\",\"year\":2003,\"color\":\"#7BC4C4\",\"pantone_value\":\"14-4811\"},{\"id\":5,\"name\":\"tigerlily\",\"year\":2004,\"color\":\"#E2583E\",\"pantone_value\":\"17-1456\"},{\"id\":6,\"name\":\"blue turquoise\",\"year\":2005,\"color\":\"#53B0AE\",\"pantone_value\":\"15-5217\"}]}";
        Response res =  given().
                spec(reqSpec).
                when().
                get("https://reqres.in/api/unknown");
        ResponseBody body = res.body();
        assertEquals(res.statusCode(), 200) ;
        assertEquals(body.asString(), jsonString);
    }

    @Test
    public void SingleResource(){
        String jsonString = "{\"data\":{\"id\":2,\"name\":\"fuchsia rose\",\"year\":2001,\"color\":\"#C74375\",\"pantone_value\":\"17-2031\"}}";
        Response res = given().
                spec(reqSpec).
                when().
                get("https://reqres.in/api/unknown/2");
        ResponseBody body = res.body();
        assertEquals(res.statusCode(), 200) ;
        assertEquals(body.asString(), jsonString);
    }

    @Test
    public void SingleResourceNotFound(){
        String jsonString = "{}";
        Response res = given().
                spec(reqSpec).
                when().
                get("https://reqres.in/api/unknown/23");
        ResponseBody body = res.body();
        assertEquals(res.statusCode(), 404) ;
        assertEquals(body.asString(), jsonString);
    }

    @Test
    public void Create(){
        requestParams.clear();
        requestParams.put("name", "morpheus");
        requestParams.put("job", "leader");

        String regex1 = "morpheus";
        String regex2 = "leader";
        Response res =  given().
                spec(reqSpec).
                when().
                body(requestParams.toJSONString()).
                post("https://reqres.in/api/users");
        ResponseBody body = res.body();

        assertEquals(res.statusCode(), 201);

        assertTrue(body.asString().contains(regex1));
        assertTrue(body.asString().contains(regex2));
    }

    @Test
    public void Update(){
        requestParams.clear();;
        requestParams.put("name", "morpheus");
        requestParams.put("job", "zion resident");
        String regex1 = "morpheus";
        String regex2 = "zion resident";

        Response res = given().
                spec(reqSpec).
                when().
                body(requestParams.toJSONString()).
                put("https://reqres.in/api/users/2");
        ResponseBody body = res.body();

        assertEquals(res.statusCode(), 200);

        assertTrue(body.asString().contains(regex1));
        assertTrue(body.asString().contains(regex2));
    }

    @Test
    public void UpdatePatch(){
        requestParams.clear();
        requestParams.put("name", "morpheus");
        requestParams.put("job", "zion resident");
        String regex1 = "morpheus";
        String regex2 = "zion resident";

        Response res = given().
                spec(reqSpec).
                when().
                body(requestParams.toJSONString()).
                put("https://reqres.in/api/users/2");
        ResponseBody body = res.body();

        assertEquals(res.statusCode(), 200);

        assertTrue(body.asString().contains(regex1));
        assertTrue(body.asString().contains(regex2));
    }

    @Test
    public void Delete(){

        Response res =  given().
                spec(reqSpec).
                when().
                delete("https://reqres.in/api/users/2");
        assertEquals(res.statusCode(), 204);

    }

    @Test
    public void RegisterSuccesses(){
        requestParams.clear();
        requestParams.put("email", "eve.holt@reqres.in");
        requestParams.put("password", "pistol");
        String jsonString = "{\"id\":4,\"token\":\"QpwL5tke4Pnpja7X4\"}";
        Response res =  given().
                spec(reqSpec).
                when().
                body(requestParams.toJSONString()).
                post("https://reqres.in/api/register");
        ResponseBody body = res.getBody();
        assertEquals(res.statusCode(), 200);

        assertEquals(body.asString(), jsonString);
    }

    @Test
    public void RegisterUnsuccessful(){
        requestParams.clear();
        requestParams.put("email", "eve.holt@reqres.in");
        String jsonString = "{\"error\":\"Missing password\"}";
        Response res =  given().
                spec(reqSpec).
                when().
                body(requestParams.toJSONString()).
                post("https://reqres.in/api/register");
        ResponseBody body = res.getBody();
        assertEquals(res.statusCode(), 400);

        assertEquals(body.asString(), jsonString);
    }

    @Test
    public void LoginSuccessful(){
        requestParams.clear();
        requestParams.put("email", "eve.holt@reqres.in");
        requestParams.put("password", "cityslicka");
        String jsonString = "QpwL5tke4Pnpja7X4";
        Response res =  given().
                spec(reqSpec).
                when().
                body(requestParams.toJSONString()).
                post("https://reqres.in/api/login");
        ResponseBody body = res.getBody();
        assertEquals(res.statusCode(), 200);

        assertTrue(body.asString().contains(jsonString) );
    }

    @Test
    public void LoginUnsuccessful(){
        requestParams.clear();
        requestParams.put("email", "peter@klaven");
        String jsonString = "{\"error\":\"Missing password\"}";
        Response res =  given().
                spec(reqSpec).
                when().
                body(requestParams.toJSONString()).
                post("https://reqres.in/api/login");
        ResponseBody body = res.getBody();
        assertEquals(res.statusCode(), 400);

        assertTrue(body.asString().contains(jsonString) );
    }

    @Test
    public void DelayedResponse(){
        String jsonString = "{\"page\":1,\"per_page\":6,\"total\":12,\"total_pages\":2,\"data\":[{\"id\":1,\"email\":\"george.bluth@reqres.in\",\"first_name\":\"George\",\"last_name\":\"Bluth\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg\"},{\"id\":2,\"email\":\"janet.weaver@reqres.in\",\"first_name\":\"Janet\",\"last_name\":\"Weaver\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg\"},{\"id\":3,\"email\":\"emma.wong@reqres.in\",\"first_name\":\"Emma\",\"last_name\":\"Wong\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/olegpogodaev/128.jpg\"},{\"id\":4,\"email\":\"eve.holt@reqres.in\",\"first_name\":\"Eve\",\"last_name\":\"Holt\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg\"},{\"id\":5,\"email\":\"charles.morris@reqres.in\",\"first_name\":\"Charles\",\"last_name\":\"Morris\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg\"},{\"id\":6,\"email\":\"tracey.ramos@reqres.in\",\"first_name\":\"Tracey\",\"last_name\":\"Ramos\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/bigmancho/128.jpg\"}]}";
        Response res =  given().
                spec(reqSpec).
                when().
                get("https://reqres.in/api/users?delay=3");
        ResponseBody body = res.getBody();
        assertEquals(res.statusCode(), 200);
        assertEquals(body.asString(), jsonString);
    }

    @AfterTest
    public void postcondition() {

    }

}
